# Laravel 5.3 Email Confirmation Step by Step 

 - php artisan make:auth 
 - change .env file database credentials  ( name - user - pass ) 
 - add these two columns to the users migration after password

```
#!php

  $table->boolean('verified')->default(false);
  $table->string('token', 40)->nullable();

```



- php artisan migrate 

- copy this function from 

vendor/laravel/framework/src/Illuminati/foundation/auth/RegisterUsers.php

and paste it at the end of RegisterController , then comment the last lines as you see , and edit it to be like this 


```
#!php

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));


      Mail::to($user->email)->send(new ConfirmationAccount($user));

      return back()->with('status', 'Please confirm your email address.');

      //  $this->guard()->login($user);

      //  return $this->registered($request, $user)
      //      ?: redirect($this->redirectPath());
    }

```


-  update the namespaces in Register Controller 

```
#!php

use App\Http\Controllers\Controller;
use App\Mail\ConfirmationAccount;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

```





- php artisan make:mail ConfirmationAccount

- in App/Mail/ConfirmationAccount.php 

  add the User namespace

```
#!php

* use App\User;

```
 
 * edit the class ConfirmationAccount  to be 


```
#!php

class ConfirmationAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $user;


     public function __construct(User $user)
       {
           $this->user = $user;
       }


    public function build()
    {
        return $this->view('emails.confirmation');
    }
}


```



- create the view (emails.confirmation)


```
#!html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up Confirmation</title>
</head>
<body>
    <h1>Thanks for signing up!</h1>

    <p>
        We just need you to <a href='{{ url("register/confirm/{$user->token}") }}'>confirm your email address
        </a> real quick!
    </p>
</body>
</html>

```




- create the view partials/flash.blade.php
 

```
#!php

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

```



- add this line in register.blade.php , login.blade.php 
after the panel heading 

```
#!php


  @include('partials.flash')

```

- go to https://mailtrap.io  and create account 
- open the Demo inbox you will find your credentials  
- edit the .env file with your mail credentials 


```
#!php


MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=yourusername 
MAIL_PASSWORD=yourpassword
MAIL_ENCRYPTION=null

```


- now run project using  php artisan serve and test registration process 
- if you created account a flash message wsill show up with this message
 " Please confirm your email address. "   , if it didn't show up go back and check your code 
----------------------------------------------------
- now we need to prevent user from login until he confirm his email 
- go to vendor/laravel/framework/src/Illuminati/foundation/auth/AuthinticateUsers.php and
edit this 

```
#!php

  protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password') + ['verified' => true] ;
    }

```



- In the User model 

add this at the end of the class 


```
#!php

    /**
    *
    * Boot the model.
    *
    */
      public static function boot()
      {
        parent::boot();

        static::creating(function ($user) {
            $user->token = str_random(40);
        });
      }

          /**
       * Confirm the user.
       *
       * @return void
       */
      public function confirmEmail()
      {
          $this->verified = true;
          $this->token = null;

          $this->save();
      }


```



- go to vendor/laravel/framework/src/Illuminati/routing/router.php

add this line in the public function auth()


```
#!php


  // Registration Routes...
        $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
        $this->post('register', 'Auth\RegisterController@register');
        $this->get('register/confirm/{token}', 'Auth\RegisterController@confirmEmail');


```

- back to register controller 

add this at the end 

```
#!php

  /**
   * Confirm a user's email address.
   *
   * @param  string $token
   * @return mixed
   */
  public function confirmEmail($token)
  {
      User::whereToken($token)->firstOrFail()->confirmEmail();

      return redirect('login')->with('status', 'You are now confirmed. Please login.');
  }


```

,,,,,,

to use Gmail instead of mailtrap 

- create gmail account 
- in https://myaccount.google.com/security
   scroll down till you find (Allow less secure apps ) and turn it on 
- in your project .env file use this credintials 

```
#!php
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=yourmail@gmail.com
MAIL_PASSWORD=yourPassword
MAIL_ENCRYPTION=tls 

```



- when working on localhost disable your antivirus and your windows firewall

,,,,,,

now it should work :) 

thanks to ZONEOFIT 
https://www.youtube.com/watch?v=aOaSZr-_uf4